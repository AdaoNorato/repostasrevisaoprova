import java.util.Arrays;
import java.util.List;

import javax.swing.plaf.basic.BasicScrollPaneUI.VSBChangeListener;

public class codigos {
    public static void main(String[] args) {
        System.out.println('\n' + "teste exercicio 3 ");
        testeNotas();

        System.out.println('\n' + "teste exercicio 5");
        testeElementosChave();

    
    }
    public static void testeElementosChave(){
        
        Integer[] array = {6,5,4,3,2,1};
        List<Integer> vs = Arrays.asList(array); 
    
        System.out.println("usando recursivo: "+elementosAteChave(vs, 3));
        System.out.println("usando for "+ elementsAtChave(vs,3));
    
    }
    public static void bonificarNotas(int[] notas){
        for( int i = 0; i< notas.length; i++){
            notas[i] += notas[i] < 10 ? 1:0;
        } 
        System.out.println(GetArrayElements(notas,notas.length));
    }
    public static int[] calcularMedias(int[] n1, int[] n2){
        int [] medias = new int[n1.length];
        for(int i = 0; i < medias.length; i++){
            medias[i] = n1[i] + n2[i] / 2;
        }
        n2 = medias;
        System.out.println(GetArrayElements(n2,n2.length));
        return n2;
    }
    public static void verificarAprovacao(int[] medias, boolean isAprovado){
        int total = 0;
        for(int media: medias){
            total += media; 
        }
        isAprovado = total > medias.length *7;
        System.out.println(isAprovado);
    }

    public static String GetArrayElements(int[] array, int tamanho){
        String elementos = "";
        for(int i = 0 ; i < tamanho ; i++){
            elementos += " " +  array[i];
        }
        return elementos;
    }  
    public static String GetArrayElements(List<Integer> vs, Integer tamanho){
        String elementos = "";
        for(int i = 0 ; i < tamanho ; i++){
            elementos += " " +  vs.get(i);
        }
        return elementos;
    }

    public static void testeNotas(){
        int[] notasBim1 = {8,7,9,4,10};
        int[] notasBim2 = {6,3,5,9,9};
        boolean isAprovado = false;
        
        bonificarNotas(notasBim1);
        calcularMedias(notasBim1, notasBim2);
        verificarAprovacao(notasBim2, isAprovado);
        
        System.out.println( GetArrayElements(notasBim1,notasBim1.length) +","+ GetArrayElements(notasBim2,notasBim2.length) + ","+isAprovado);
    }

    
    public static int elementosAteChave(List<Integer> algarismo, Integer chave){
        if(algarismo.size() <= 0 || algarismo.get(0)==chave) return 0;
        final List<Integer> subAlgarismo = algarismo.subList(1, algarismo.size());
        System.out.println(GetArrayElements(subAlgarismo, subAlgarismo.size()));
        return 1 + elementosAteChave (subAlgarismo,chave);
    }


    public static int elementsAtChave(List<Integer> algarismo, Integer chave){
        int index = 0;
        for(int i = 0; i<algarismo.size(); i++){
            if( algarismo.get(i) == chave){
                index = i;
                break;
            }else{
                index = algarismo.size();
            }
        }
        return index;
    }

}