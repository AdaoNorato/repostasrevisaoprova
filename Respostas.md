# Revisão: prova Adão Norato Claro
# 1)
![img](https://gitlab.com/AdaoNorato/repostasrevisaoprova/-/raw/master/assets/res1.png)
<br> ordA: Bolha, compara o maior elemento,
se for maior troca de lugar com o menor elemento.<br>
<br> ordB: Inserção, divide os elementos em duas seções, se organiza os da esquerda, depois junta com o próximo elemento da direita.<br>
<br> ordC: Seleção, divide os elementos em duas seções assim como inserção, e joga o menor elemento da seção desorganizada no primeiro espaço da seção desorganizada, o primeiro elemento da seção desorganizada agora foi organizado e se reperete até todos elementos estivem organizados.

# 2)
![img](https://gitlab.com/AdaoNorato/repostasrevisaoprova/-/raw/master/assets/res2.png)

questão cancelada, mas o metodo bolha ainda faz menos trocas que o seleção, faz mais trocas que o inserção.

# 3) 
<details>
    <summary> Clique para mostrar código. </summary>
    
    ```
    public static void bonificarNotas(int[] notas){
        for( int i = 0; i< notas.length; i++){
            notas[i] += notas[i] < 10 ? 1:0;
        } 
        System.out.println(GetArrayElements(notas,notas.length));
    }

    public static int[] calcularMedias(int[] n1, int[] n2){
        int [] medias = new int[n1.length];
        for(int i = 0; i < medias.length; i++){
            medias[i] = n1[i] + n2[i] / 2;
        }
        n2 = medias;
        System.out.println(GetArrayElements(n2,n2.length));
        return n2;
    }
    
    public static void verificarAprovacao(int[] medias, boolean isAprovado){
        int total = 0;
        for(int media: medias){
            total += media; 
        }
        isAprovado = total > medias.length *7;
        System.out.println(isAprovado);
    }


    public static void main(String[] args) {
        int[] notasBim1 = {8,7,9,4,10};
        int[] notasBim2 = {6,3,5,9,9};
        boolean isAprovado = false;

        bonificarNotas(notasBim1);
        calcularMedias(notasBim1, notasBim2);
        verificarAprovacao(notasBim2, isAprovado);
        
        System.out.println( GetArrayElements(notasBim1,notasBim1.length) +","+ GetArrayElements(notasBim2,notasBim2.length) + ","+isAprovado);
    }

    public static String GetArrayElements(int[] array, int tamanho){
        String elementos = "";
        for(int i = 0 ; i < tamanho ; i++){
            elementos += " " +  array[i];
        }
        return elementos;
    }
    ```
</details>
<br> a = 
<br> 9 8 10 5 10 <br>
<br> b = 
<br> 12 9 12 9 14 <br>
<br> c = 
<br> false <br>
<br> d = 
<br> 9 8 10 5 10, 6 3 5 9 9, false <br>

# 4) 
![img](https://gitlab.com/AdaoNorato/repostasrevisaoprova/-/raw/master/assets/res3.png)
o metodo é chamado com uma lista "VS" e o valor "K".
o algoritimo recursivo vai retornar a quantidade de elementos até encontrar K.
<br> vs = {6,5,4,3,2,1} <br> K =  3  <br> retorna = 3 <br> <br> justificando a resposta: pra encontrar K se cria uma sub-lista e remove o primeiro elemento se ele não é K. <br> exemplo: se "vs" é a mesma e K = 6,  ele retorna 0, pois K = 1º elemento da lista, ou, indice 0.
# 5) 
<br> nomes mais descritivo para o metodo:
![resposta5a](https://gitlab.com/AdaoNorato/repostasrevisaoprova/-/raw/master/assets/res5b.png)
<details>
<summary> Clique para mostrar código </summary>

    public static void main(String[] args) {

        Integer[] array = {6,5,4,3,2,1};
        List<Integer> vs = Arrays.asList(array); 

        System.out.println(elementosAteChave(vs,6));

    }
    
    public static int elementosAteChave(List<Integer> algarismo, Integer chave){
        if(algarismo.size() <= 0 || algarismo.get(0)==chave) return 0;
        final List<Integer> subAlgarismo = algarismo.subList(1, algarismo.size());
        System.out.println(GetArrayElements(subAlgarismo, subAlgarismo.size()));
        return 1 + elementosAteChave (subAlgarismo,chave);
    }

<details>
<summary> metodo GetArrayElements </summary>

    public static String GetArrayElements   (List<Integer> vs, Integer tamanho){
    String elementos = "";
    for(int i = 0 ; i < tamanho ; i++){
    elementos += " " +  vs.get(i);
    }
    return elementos;
    }

</details>

<br> Valor da sub-list print:<br>  5 4 3 2 1
<br> 4 3 2 1
<br> 3 2 1
<br> return 3
</details>

Usando metodo for:
![resposta5b](https://gitlab.com/AdaoNorato/repostasrevisaoprova/-/raw/master/assets/res5b.png) 
<details>
<summary> Clique para ver o código. </summary>
    
    public static int elementsAtChave(List<Integer> algarismo, Integer chave){
        int index = 0;
        for(int i = 0; i<algarismo.size(); i++){
            if( algarismo.get(i) == chave){
                index = i;
                break;
            }else{
                index = algarismo.size();
            }
        }
        return index;
    }
</details>

# 6 
Metodo Recursivo, é passado um valor inteiro ou BigInteger, e retorna a somatória de todos os numeros até 0.
Usar int poderia dar um Overflow muito rapido, se usa o BigInteger para o melhor desempenho do metodo. <br>
@params x : numero que você queira receber o fatorial. 


    public static BigInteger Fatorial(BigInteger x)
        if(x<=1)
            return 1;
        else
            return x + Fatorial(x-1) ;
    }

# 7
![quadrado](https://gitlab.com/AdaoNorato/repostasrevisaoprova/-/raw/master/assets/res7.png)
<details> <summary> 
    Clique para mostrar o código do quadrado.     
    </summary>
        
        
        public static void DesenharQuad(Graphics2D pincel, Dimension dim) {
        final int quad = (int) ((.1f * dim.width + .1f * dim.height ) / 2 ) ;
        final int maxquad = quad * 4;

        //Quad externo
        pincel.drawLine(quad  , quad , maxquad  , quad   );//x
        pincel.drawLine(quad  , quad , quad  , maxquad   );//y
        pincel.drawLine(quad  , maxquad , maxquad  , maxquad   );//x
        pincel.drawLine(maxquad  , quad , maxquad  , maxquad   );//y

        //Quad meio
        pincel.drawLine(quad + (quad/2) , quad+ (quad/2) , maxquad - (quad/2) , quad + (quad/2)  );//x
        pincel.drawLine(quad+ (quad/2)  , quad + (quad/2), quad+ (quad/2)  , maxquad - (quad/2)   );//y
        pincel.drawLine(quad + (quad/2) , maxquad - (quad/2)  , maxquad - (quad/2)  , maxquad  - (quad/2)  );//x
        pincel.drawLine(maxquad - (quad/2)  , quad + (quad/2), maxquad - (quad/2)  , maxquad - (quad/2)   );//y

        //Quad interno
        pincel.drawLine(quad *2 , quad*2 , maxquad-quad  , quad*2   );//x
        pincel.drawLine(quad *2 , quad*2 , quad * 2 , maxquad - quad );//y
        pincel.drawLine(quad*2  ,  maxquad - quad ,  maxquad - quad  ,  maxquad - quad   );//x
        pincel.drawLine( maxquad - quad  , quad*2 ,  maxquad - quad  ,  maxquad - quad   );//y
        }
</details>
